<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //function memanggil route create
    public function create()
    {
        return view('cast.create');
    }

    //function memanggil route store
    public function store(Request $request)
    {
        //dd digunakan untuk check apakah data berhasil masuk
        //dd($request->all());

        //code diambil dari laravel docs 6.x validation
        //digunakan untuk mewajibkan user mengisi inputan
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required|min:10',
        ],
        [
            'nama.required' => 'nama harus diisi',
            'nama.min' => 'nama harus lebih dari 1 karakter',
            'umur.required' => 'nama harus diisi',
            'bio.required' => 'nama harus diisi',
            'bio.min' => 'Bio harus lebih dari 10 karakter',
        ]
    );

        //code diambil dari laravel docs 6.x
        //digunakan untuk memasukkan data ke db
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'], 
                'umur' => $request['umur'], 
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');

    }

    public function index()
    {
        //digunakan untuk mengambil data
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required',
            'bio' => 'required|min:10',
        ],
        [
            'nama.required' => 'nama harus diisi',
            'nama.min' => 'nama harus lebih dari 1 karakter',
            'umur.required' => 'nama harus diisi',
            'bio.required' => 'nama harus diisi',
            'bio.min' => 'Bio harus lebih dari 10 karakter',
        ]
    );

    DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
