<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view('page.auth');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $first = $request['firstname'];
        $last = $request['lastname'];

        return view('welcome' , compact('first','last'));
    }
}
