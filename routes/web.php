<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@auth');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-table' , function(){
    return view('page.data-table');
});

Route::get('/table' , function(){
    return view('page.table');
});

//CRUD cast

//Create
//Route untuk menuju ke form pembuatan tambah cast
Route::get('/cast/create','CastController@create');

//Menyimpan database
Route::post('/cast','CastController@store');

//Read database
Route::get('/cast','CastController@index');

//Detail cast berdasarkan id
Route::get('/cast/{cast_id}','CastController@show');

//Untuk update data mengarah ke form edit cast
Route::get('/cast/{cast_id}/edit','CastController@edit');

//Untuk update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update' );

//Untuk delete berdasarkan paramater
Route::delete('/cast/{cast_id}', 'CastController@destroy' );