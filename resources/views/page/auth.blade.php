@extends('layout.master')
@section('title')
Form Page
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
    <h2>Sign up Form</h2>

    <form action="/welcome" method="post">
        @csrf
        <label> First name :</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name :</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="lang">Bahasa Indonesia<br>
        <input type="checkbox" name="lang">English<br>
        <input type="checkbox" name="lang">Other<br><br>
        <label>Bio: </label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up"></button>

    </form>
@endsection