@extends('layout.master')
@section('title')
Halaman edit cast
@endsection
@section('content')
<h1>Buat Cast Baru</h1>
    
<!-- form diberi method post dengan link ke /cast -->
<form method="POST" action="/cast/{{$cast->id}}">
    <!-- jangan lupa token ! -->
    @csrf
    <!-- Menyuruh laravel membaca menjadi put -->
    @method('put')
    <!-- warning validation -->
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <!-- input nama -->
    <div class="form-group">
      <label >Nama Cast</label>
      <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
    </div>

    <!-- warning validation -->
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <!-- input umur -->
    <div class="form-group">
      <label >Umur</label>
      <input type="number" class="form-control" value="{{$cast->umur}}" name="umur">
    </div>

    <!-- warning validation -->
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <!-- input bio -->
    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
      </div>
      
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection